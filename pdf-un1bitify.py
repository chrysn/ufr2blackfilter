#!/usr/bin/env python3

import sys
import functools
import argparse
import io

import PyPDF2

print = functools.partial(print, file=sys.stderr)

reader = PyPDF2.PdfFileReader(io.BytesIO(sys.stdin.buffer.read()))

substitute = {}

# FIXME is the 0 i'm hardcodeing here and there the generation? then i should iterate over it, shouldn't i?

for xref in reader.xref[0].keys():
    xobject = reader.getObject(PyPDF2.generic.IndirectObject(xref, 0, reader))

    if not isinstance(xobject, PyPDF2.generic.EncodedStreamObject) or xobject.get('/BitsPerComponent') != 1:
        continue

    # we can't setData on an encoded stream object, so let's make a decoded one
    # and later exchange them

    xobject.getData() # make decodedSelf appear
    xobject = xobject.decodedSelf
    substitute[xref] = xobject

    # native strings or numerals don't work directly, build or extract them
    eight = PyPDF2.generic.NumberObject(8)
    bitspercomponent, = [k for k in xobject if k == '/BitsPerComponent']
    xobject[bitspercomponent] = eight

    width = int(xobject['/Width'])
    bytewidth = (width - 1) // 8 + 1
    print(width, bytewidth)
    assert (bytewidth - 1) * 8 < width <= bytewidth * 8 # i don't trust my modulo fiddling

    def to_eightbytes(b):
        """Given a byte, return a sequence of eight 0x00 or 0xff bytes that
        correspond to the input byte's bits"""
        return "{0:08b}".format(b).encode('ascii').replace(b'0', b'\0').replace(b'1', b'\xff')
    def chunk_up(bytestring, slicesize):
        assert len(bytestring) % slicesize == 0
        for i in range(len(bytestring) // slicesize):
            yield bytestring[slicesize * i:slicesize * (i + 1)]
    xobject.setData(b"".join(
        b"".join(to_eightbytes(b) for b in line)[:width]
        for line
        in chunk_up(xobject.getData(), bytewidth)))

for xref in reader.xref[0].keys():
    xobject = reader.getObject(PyPDF2.generic.IndirectObject(xref, 0, reader))

    if isinstance(xobject, PyPDF2.generic.DictionaryObject) and '/XObject' in xobject:
        for k, v in list(xobject['/XObject'].items()):
            if v.idnum in substitute and v.generation == 0: # the 'generation == 0' part is only for me to remember that when i start using them, i've got to touch this
                xobject['/XObject'][k] = substitute[v.idnum]

writer = PyPDF2.PdfFileWriter()

for p in range(reader.getNumPages()):
    writer.addPage(reader.getPage(p))

outbuf = io.BytesIO()
writer.write(outbuf)

sys.stdout.buffer.write(outbuf.getbuffer())
