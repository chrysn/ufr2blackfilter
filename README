This is a hack around a bug in Canon's pstoufr2cpca printer driver where images
sent in PostScript as 1-bit-per-pixel are interpreted as 8-bit-per-pixel,
resulting in a largely black output image with small (possibly skewed) copies
of the image in the first row. That happens with my MF4500 series printer, and
has been reported with MF5900 too[1].

The fix consists of two parts: One PDF processor (written in Python 3, using
PyPDF2) that replaces all 1-bit-per-pixel images in the PDF with an
8-pit-per-pixel version, and a CUPS filter that can be configured in the PPD
file in place of the original pstoufr2cpca program by replacing the

    *cupsFilter: "application/vnd.cups-postscript 0 pstoufr2cpca"

line with

    *cupsFilter: "application/pdf 0 /path/to/pstoufr2cpca-preprocessor"

This is just being tested with real-world PDFs, and I'm abusing PyPDF2 in
several ways which it seems not to have been designed for. I'd appreciate any
feedback on the methods used here.

---

All code in here was written by chrysn <chrysn@fsfe.org> in 2017 and is
published under the terms of CC0[2].

[1]: https://bbs.archlinux.org/viewtopic.php?id=216039
[2]: http://creativecommons.org/publicdomain/zero/1.0/
